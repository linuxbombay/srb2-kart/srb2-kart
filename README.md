![picture](https://cdn2.steamgriddb.com/logo_thumb/bad245e5c06461bfd1e49b4a03fcf441.png) 

A repository for SRB2Kart, a kart racing mod based on the 3D Sonic the Hedgehog fangame Sonic Robo Blast 2, based on a modified version of Doom Legacy.

All binaries are compiled from the official project
https://github.com/STJr/Kart-Public

Repository for AUR package
Packages:
[srb2kart-bin](https://aur.archlinux.org/packages/srb2kart-bin))

 ### Author
  * Corey Bruce
